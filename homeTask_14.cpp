﻿#include <iostream>
#include <string>

int main()
{
    std::string line;
    int lineCount, lastIngex;
    std::cout << "Enter your string: ";
    std::getline(std::cin, line);
    std::cout << "\n";
    lineCount = line.length();
    lastIngex = lineCount - 1;
    std::cout << "Your string is: " << line << "\n";
    std::cout << "There are " << lineCount << " chars" << "\n";
    std::cout << "First char: " << line[0] << "\n";
    std::cout << "Last char: " << line[lastIngex] << "\n";
}